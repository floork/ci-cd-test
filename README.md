# Prize Draw

## Requirements

```py
python -m pip install aiohttp click
```

## Usage

```sh
python main.py <organizations or repos>
```

repo format: `owner/repo`

## Parameters

| Options | Description |
| --- | ----------- |
| `--github-token` or `-t` | add a github token |
| `--number-of-winners` or `-w` | set amount of winners |
| `--show-names` or `-s` | Show the names of the stargazers with their frequency |
