import asyncio
from collections import defaultdict
import random
from typing import List, DefaultDict, Optional
import sys
import aiohttp
import click


class RandomWinnerPicker:
    def __init__(
        self,
        github_token: Optional[str] = None,
        number_of_winners: int = 1,
        show_names: bool = False,
        repositories: Optional[List[str]] = None,
    ) -> None:
        self.github_token = github_token
        self.number_of_winners = number_of_winners
        self.show_names = show_names
        self.repositories = repositories
        self.headers = {
            "Accept": "application/vnd.github+json",
            "X-GitHub-Api-Version": "2022-11-28",
        }
        if self.github_token is not None:
            self.headers["Authorization"] = f"Bearer {self.github_token}"

    async def get_data(self, url: str, index: str) -> list[str]:
        data: List[str] = []
        per_page = 100

        async with aiohttp.ClientSession() as session:
            for page in range(1, sys.maxsize):
                params = {"page": page, "per_page": per_page}
                async with session.get(
                    url, headers=self.headers, params=params
                ) as response:
                    response_data = await response.json()
                    if len(response_data) == 0:
                        break
                    data.extend(
                        item[index] for item in response_data if index in item
                    )

        return data

    async def all_repos(self, orga_name: str) -> list[str]:
        """Get all repositories of the organization"""
        url = f"https://api.github.com/orgs/{orga_name}/repos"
        return await self.get_data(url, "name")

    async def repo_stargazers(self, repo: str) -> defaultdict[str, int]:
        """Get all stargazers of a repository"""
        url = f"https://api.github.com/repos/{repo}/stargazers"
        users = await self.get_data(url, "login")
        stargazers: DefaultDict[str, int] = defaultdict(int)

        for login in users:
            stargazers[login] += 1

        return stargazers

    async def orga_stargazers(
        self, orga: str, repos: list[str]
    ) -> defaultdict[str, int]:
        """get all stargazers of a repo"""
        stargazers: DefaultDict[str, int] = defaultdict(int)

        for repo in repos:
            temp_stargazers = await self.get_stargazers(f"{orga}/{repo}")
            for login in temp_stargazers.keys():
                stargazers[login] += 1

        return stargazers

    async def get_stargazers(self, repo: str) -> defaultdict[str, int]:
        """returns all stargazers"""
        if "/" not in repo:
            orga = repo
            repos = await self.all_repos(orga)
            return await self.orga_stargazers(orga, repos)

        return await self.repo_stargazers(repo)

    async def stargazer_count(self) -> defaultdict[str, int]:
        """Count the number of stargazers for each login"""
        if self.repositories is None:
            raise ValueError("No repositories given!")
        else:
            stargazers_list = await asyncio.gather(
                *(self.get_stargazers(repo) for repo in self.repositories)
            )

            stargazers: DefaultDict[str, int] = defaultdict(int)

            for stargazers_dict in stargazers_list:
                for login, count in stargazers_dict.items():
                    stargazers[login] += count

            print(f"Total number of stargazers: {len(stargazers)}")

            return stargazers

    def generate_prize_list(
        self, stargazers: defaultdict[str, int]
    ) -> list[str]:
        """Generate a list of possible winners"""
        prize_list = []

        if self.show_names:
            print("Possible winners:", end=" ")
            print(
                ", ".join(
                    f"{key} {value}"
                    for key, value in sorted(
                        stargazers.items(), key=lambda item: item[1]
                    )
                )
            )

        for login, count in stargazers.items():
            prize_list.extend([login] * count)

        return prize_list

    def get_winners(
        self, prize_list: list[str], number_of_winners: int
    ) -> list[str]:
        """Pick winners from the prize list"""
        winners: List[str] = []
        while len(winners) < number_of_winners:
            winner: str = random.choice(prize_list)
            if winner not in winners:
                winners.append(winner)
                prize_list.remove(winner)
        return winners

    def check_if_to_many_winners(
        self, prize_list: list[str], number_of_winners: int
    ) -> None:
        if len(prize_list) < number_of_winners:
            raise ValueError(f"Only {len(prize_list)} possible winners!")

    def pick_random_winners(self) -> None:
        asyncio.run(self.amain())

    def print_winners(self, winners: list[str]) -> None:
        if len(winners) > 1:
            winners_str = ", ".join(winners)
            print(f"The winners are: {winners_str}!")
        else:
            winner = winners[0]
            print(f"The winner is {winner}")

    async def amain(self) -> None:
        prize_list = self.generate_prize_list(await self.stargazer_count())
        self.check_if_to_many_winners(prize_list, self.number_of_winners)
        winners_list = self.get_winners(prize_list, self.number_of_winners)
        self.print_winners(winners_list)


@click.command()
@click.option("--github-token", "-t", required=False, help="GitHub token")
@click.option(
    "--number-of-winners", "-w", default=1, type=int, help="Number of winners"
)
@click.option(
    "--show-names",
    "-s",
    is_flag=True,
    help="Show the names of the stargazers with their counts",
)
@click.argument("repositories", nargs=-1)
def main(
    github_token: Optional[str] = None,
    number_of_winners: int = 1,
    show_names: bool = False,
    repositories: Optional[List[str]] = None,
) -> None:
    winner_picker = RandomWinnerPicker(
        github_token, number_of_winners, show_names, repositories
    )
    winner_picker.pick_random_winners()


if __name__ == "__main__":
    main()
